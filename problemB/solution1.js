const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function factorial(x) {
    if(x == 0 || x == 1) return 1;
    for(let y = x - 1; y >= 1; y--){ x *= y; }
    // console.log(x);
    return x;
}

let dictionary = {};
function repeatingChars(string){
    for(let x = 0; x < string.length; x++){
        const char = string.charAt(x);
        // check if the letter is repeated or not
        // if it isnt, make it count 1
        if(!dictionary[char]){ dictionary[char] = 1; }
        // if it is, count it up.
        else{ dictionary[char]++; }
    }
    return dictionary;
}

rl.on('line', (line) => {
    const s = line;
    repeatingChars(s);
    const entries = Object.entries(dictionary);
    // console.log('entries: ', entries);
    let division = 1;
    for(const [key, count] of entries){
        // console.log(factorial(count));
        division *= factorial(count);
        // console.log(`key: ${key}: ${count}`);
    }
    // console.log('division: ', division);

    const upper = factorial(s.length);
    const result = upper / division;
    // console.log('result: ', result);
    dictionary = {};
    console.log(result);
})
.on('close', (line) => {
    // EOF
});