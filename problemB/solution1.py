import sys
import math

for line in sys.stdin:
    string = line.rstrip()

    dictionary = {}
    # this is the same as the repeatChars function in the Javascript solution
    for char in string:
        if not char in dictionary:
            dictionary[char] = 1
        else:
            dictionary[char] += 1

    # print(dictionary)
    division = 1
    # this is the same as [key, vlaue] for looop in the javascript
    for value in dictionary.values():
        # print(value)
        division *= math.factorial(value)
        # print(division)

    upper = math.factorial(len(string))
    # print(upper)
    # apparentrtly this is wrong answer as its imprecise
    # result = upper / division
    # this is integer division so its more precise apparently
    result = upper // division
    print(int(result))