import sys

dataLeft = 0
travelDistance = 0
lastTime = 0

for line in sys.stdin:
    if(dataLeft == 0):
        dataLeft = int(line)
        travelDistance = 0
        lastTime = 0
    else:
        data = line.split()
        speed = int(data[0])
        time = int(data[1])
        if(speed == -1):
            sys.exit()
        dataLeft -= 1
        travelDistance += speed * (time - lastTime)
        lastTime = time
        if(dataLeft == 0):
            print(travelDistance, ' miles')