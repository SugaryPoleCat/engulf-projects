const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

// I decided to seperate them into two arrays and use a set.
let set;
const speeds = [];
const times = [];

/**
 * This will run the calculation, of how many miles Bill and Ted drove in their magical time machine.
 * it will return the console.log, printing the result.
 */
function calculate(){
    let result = 0;
    const results = [];
    if(speeds.length == 0){ return; }
    for(let x = 0; x < set; x++){
        // this is the time calculation. It takes the index and subtractrs the time from previous index, to get the time difference correctly. If it's the first entry, the difference is 0.
        result = speeds[x] * (times[x] - (times[x - 1] || 0));
        // push the result to an array to be later added all at once.
        results.push(result);
    }
    result = 0;
    for(let x = 0; x < set; x++){
        result = result + results[x];
    }
    return console.log(result + ' miles');
}

rl.on('line', (line) => {
    // the first input is how many sets in a dataset
    // the next amount of inputs have to be 2 values in a line
    // they have to be multiplied, the speed with time from the same line, then added.

    const data = line.split(' ');
    const a = parseInt(data[0]);
    const b = parseInt(data[1]);
    if(isNaN(b)){
        // I decided that because we use 1 number to mark a set, we can use that to run the calculate function.
        calculate();
        set = a;
        speeds.length = 0;
        times.length = 0;
    }
    else{
        // else add the pairs to seperate arrays.
        speeds.push(a);
        times.push(b);
    }
    if(a == -1){ rl.close(); }
});

// This resulted in a wrong answer. 
// I suppose its because i used a wrong way to soplve it?