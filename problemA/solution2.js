const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

// Set initial values
let datasetsLeft = 0,
curDistance = 0,
lastTime = 0;

rl.on('line', (line) => {
    const data = line.split(' ');
    const speed = parseInt(data[0]);
    const time = parseInt(data[1]);
    // check if the exit code by input is issued.
    if(speed == -1){ rl.close(); }
    // if datasets, equals 0, get a new dataset value and reset current distance and last time used.
    if(datasetsLeft === 0){
        datasetsLeft = parseInt(line);
        curDistance = 0;
        lastTime = 0;
    }
    else{
        // per each entry, take away a number from datasets.
        // So if you have 3 sets, first type in 3.
        // datasetsLeft then gets set with 3.
        // every time you input something, we subtract from that.
        // until it reaches 0, when it does it automatically prints current distance.
        datasetsLeft--;
        // this calculates the distance travelled, based on the specifications.
        curDistance += speed * (time - lastTime);
        lastTime = time;
        if(datasetsLeft === 0){ console.log(curDistance + ' miles'); }
    }
});